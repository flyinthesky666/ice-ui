function log(str) {
    console.log(str)
}

function getheight(id) {
    var h = document.getElementById(id).style.height;
    return h;
}

function getwidth(id) {
    var w = document.getElementById(id).style.width;
    return w;
}

function tosize(id, h, w) {
    document.getElementById(id).style.height = h;
    document.getElementById(id).style.width = w;
}

function swsize(id, h1, w1, h2, w2) {
    if (getheight(id) == h1 && getwidth(id) == w1) {
        tosize(id, h2, w2)
    } else if (getheight(id) == h2 && getwidth(id) == w2) {
        tosize(id, h1, w1)
    } else {
        tosize(id, h2, w2)
    }
}